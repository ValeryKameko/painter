﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Painter
{
    public class ToolButton : Panel
    {
        private String name;
        public new String Name
        {
            get => name;
            set
            {
                name = value;
                label.Text = value;
                label.Update();
            }
        }
        public Boolean Choosed { get; private set; }

        public event EventHandler ChooseEvent;

        private Label label;

        private AbstractTool tool;

        public ToolButton(AbstractTool tool)
        {
            label = new Label()
            {
                Dock = DockStyle.Fill,
                TextAlign = ContentAlignment.MiddleCenter,

                Height = 20,
                ForeColor = Color.Black
            };
            label.Click += (sender, e) => Choose(); 
            Controls.Add(label);

            this.tool = tool;

            Name = tool.Name;
            Height = 50;
            BackColor = Color.White;

            Click += (sender, e) => Choose();
            ChooseEvent += (sender, e) => tool.Enable();
            tool.DisableEvent += (sender, e) => this.Unchoose();

            Unchoose();
            Refresh();
        }
        
        public void Choose()
        {
            if (!Choosed)
            {
                BackColor = Color.Gray;
                Invalidate();
                Choosed = true;
                ChooseEvent.Invoke(this, EventArgs.Empty);
            }
        }

        public void Unchoose()
        {
            if (Choosed)
            {
                BackColor = Color.White;
                Invalidate();
                Choosed = false;
            }
        }
        
    }
}
