﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Painter.Graphics
{
    public abstract class AbstractShapeDrawer
    {
        public abstract void Down(float x, float y);
        public abstract void Up(float x, float y);
        public abstract void Move(float x, float y);
        public abstract void Flush();

        public abstract event EventHandler DrawingCompleteEvent;
        public abstract bool IsDrawingComplete { get; }
        public abstract AbstractShape PartialShape { get; }
        public abstract AbstractShape CompleteShape { get; }

        public Pen Pen { get; set; }
    }
}
