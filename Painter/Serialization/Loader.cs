﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Painter.Serialization
{
    public class Loader
    {
        public Painter application;

        public Loader(Painter application)
        {
            this.application = application;
        }

        public void Load()
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = CreateFileFilter();
            dialog.InitialDirectory = Application.StartupPath;
            var result = dialog.ShowDialog();
            if (result != DialogResult.OK)
                return;
            try
            {
                using (var stream = new FileStream(dialog.FileName, FileMode.Open))
                {
                    var serializer = application.SerializerDeserializerRegistry.ElementAt(dialog.FilterIndex - 1);
                    var shapeCollection = serializer.Deserialize(stream);
                    if (serializer.WasError)
                    {
                        MessageBox.Show(FormatLoadError(serializer.Error), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    } else
                    {
                        application.ShapeCollection.Clear();
                        shapeCollection.ToList().ForEach(application.ShapeCollection.Add);
                        if (serializer.WasWarning)
                            MessageBox.Show("Warning during save loading: " + serializer.Warning, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show(FormatLoadError("Save file not found"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (SecurityException)
            {
                MessageBox.Show(FormatLoadError("Not enough permissions"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show(FormatLoadError("Unauthorized access"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private String FormatLoadError(String error)
        {
            return "Save has not been loaded: " + error;
        }

        private String CreateFileFilter()
        {
            var builder = new StringBuilder();
            foreach (var entry in application.SerializerDeserializerRegistry)
            {
                builder.Append(entry.Desctiption);
                builder.Append("|");
                builder.Append(String.Join(";", entry.Extensions.Select(extension => $"*.{extension}")));
            }
            return builder.ToString();
        }

    }
}
