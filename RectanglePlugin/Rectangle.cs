﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Painter.Graphics;
using System.Runtime.Serialization;

namespace RectanglePlugin
{
    [DataContract]
    public class Rectangle : AbstractShape, ISelectable
    {
        [DataMember]
        private float left;
        [DataMember]
        private float right;
        [DataMember]
        private float top;
        [DataMember]
        private float bottom;

        public float Left
        {
            get => left;
            set
            {
                left = value;
                Normalize();
            }
        }
        public float Right
        {
            get => right;
            set
            {
                right = value;
                Normalize();
            }
        }
        public float Top
        {
            get => top;
            set
            {
                top = value;
                Normalize();
            }
        }
        public float Bottom
        {
            get => bottom;
            set
            {
                bottom = value;
                Normalize();
            }
        }

        public Point LeftTop
        {
            get => new Point(left, top);
            set
            {
                left = value.X;
                top = value.Y;
                Normalize();
            }
        }
        public Point LeftBottom
        {
            get => new Point(left, bottom);
            set
            {
                left = value.X;
                bottom = value.Y;
                Normalize();
            }
        }
        public Point RightTop
        {
            get => new Point(right, top);
            set
            {
                right = value.X;
                top = value.Y;
                Normalize();
            }
        }
        public Point RightBottom
        {
            get => new Point(right, bottom);
            set
            {
                right = value.X;
                bottom = value.Y;
                Normalize();
            }
        }

        public float Width => right - left;
        public float Height => bottom - top;

        public override BoundingBox BoundingBox => new BoundingBox(Left, Bottom, Right, Top);

        public override Point Origin
        {
            get => new Point((left + right) / 2, (top + bottom) / 2);
            set {
                var width = Width;
                var height = Height;
                left = value.X - width / 2;
                right = value.X + width / 2;
                top = value.Y - height / 2;
                bottom = value.Y + height / 2;
            }
        }

        public Rectangle(Point firstCorner, Point secondCorner)
        {
            left = firstCorner.X;
            right = secondCorner.X;
            top = firstCorner.Y;
            bottom = secondCorner.Y;
            Normalize();
        }

        public Rectangle()
            : this(new Point(0f, 0f), new Point(0f, 0f))
        {
        }

        private void Normalize()
        {
            var left = Math.Min(this.left, this.right);
            var right = Math.Max(this.left, this.right);
            var top = Math.Min(this.bottom, this.top);
            var bottom = Math.Max(this.bottom, this.top);
            this.left = left;
            this.right = right;
            this.top = top;
            this.bottom = bottom;
        }

        public override void Draw(System.Drawing.Graphics graphics)
        {
            graphics.DrawLine(Pen.ToDrawingPen(), LeftTop.ToDrawingPointF(), RightTop.ToDrawingPointF());
            graphics.DrawLine(Pen.ToDrawingPen(), RightTop.ToDrawingPointF(), RightBottom.ToDrawingPointF());
            graphics.DrawLine(Pen.ToDrawingPen(), RightBottom.ToDrawingPointF(), LeftBottom.ToDrawingPointF());
            graphics.DrawLine(Pen.ToDrawingPen(), LeftBottom.ToDrawingPointF(), LeftTop.ToDrawingPointF());
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            Normalize();
        }

        public override object Clone()
        {
            return new Rectangle(LeftTop, RightBottom);
        }

        public void Move(float dx, float dy)
        {
            left += dx;
            right += dx;
            top += dy;
            bottom += dy;
        }

        public bool IsSelected(float x, float y)
        {
            return (left <= x) && (x <= right) && (top <= y) && (y <= bottom);
        }

        public bool IsSelected(float x0, float y0, float x1, float y1)
        {
            return (x0 <= left) && (right <= x1) && (y0 <= top) && (bottom <= y1);
        }
    }
}
