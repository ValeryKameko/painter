﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Painter.Graphics
{
    public interface ISelectable
    {
        bool IsSelected(float x, float y);
        bool IsSelected(float x0, float y0, float x1, float y1);
    }
}
