﻿using Painter;
using Painter.Graphics;
using Painter.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RectanglePlugin
{
    public class RectangleTool : AbstractShapeDrawerTool
    {
        public RectangleTool(Painter.Painter application) : base(new RectangleDrawer(), application)
        {
        }

        public override string Name => "Rectangle";
    }
}
