﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Runtime.Serialization;

namespace Painter.Graphics
{
    [DataContract]
    public class Pen
    {
        [DataMember]
        public Color Color { get; set; }
        [DataMember]
        public float Width { get; set; }

        public Pen(Color color, float width)
        {
            Color = color;
            Width = width;
        }
        
        public System.Drawing.Pen ToDrawingPen()
        {
            var pen = new System.Drawing.Pen(Color.ToDrawingColor(), Width);
            return pen;
        }
    }
}
