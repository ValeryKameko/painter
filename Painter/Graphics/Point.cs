﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Painter.Graphics
{
    [DataContract]
    public class Point
    {
        [DataMember]
        public float X { get; set; }
        [DataMember]
        public float Y { get; set; }

        public Point(float x, float y)
        {
            X = x;
            Y = y;
        }

        public Point()
            : this(0f, 0f)
        {
        }

        public System.Drawing.PointF ToDrawingPointF() {
            return new System.Drawing.PointF(X, Y);
        }

        public System.Drawing.Point ToDrawingPoint()
        {
            return new System.Drawing.Point((int) X, (int) Y);
        }
    }
}
