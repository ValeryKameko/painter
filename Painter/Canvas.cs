﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using Painter.Graphics;

namespace Painter
{
    public class Canvas : PictureBox
    {
        public Layer DrawingLayer { get; private set; }
        public Layer UILayer { get; private set; }

        public Canvas()
        {
            BackColor = System.Drawing.Color.White;
            Paint += self_Paint;
            Resize += self_Resize;

            DrawingLayer = new Layer(Width, Height);

            UILayer = new Layer(Width, Height);
        }

        private void self_Resize(object sender, EventArgs e)
        {
            DrawingLayer.Resize(Width, Height);
            UILayer.Resize(Width, Height);
        }

        private void self_Paint(object obj, PaintEventArgs e)
        {
            var graphics = e.Graphics;
            if (DrawingLayer.IsCleared)
                DrawingLayer.Redraw();
            DrawingLayer.Draw(graphics);

            if (UILayer.IsCleared)
                UILayer.Redraw();
            UILayer.Draw(graphics);
        }
    }
}
