﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Painter.Graphics;
using System.Collections.ObjectModel;

namespace Painter
{
    public class ShapeCollection : ObservableCollection<AbstractShape>
    {
        public ShapeCollection()
        {
        }

        public void Draw(System.Drawing.Graphics graphics)
        {
            foreach (var shape in this)
            {
                shape.Draw(graphics);
            }
        }
    }
}
