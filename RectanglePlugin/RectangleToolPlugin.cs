﻿using Painter.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Painter;

namespace RectanglePlugin
{
    public class RectangleToolPlugin : IToolPlugin
    {
        private Painter.Painter application;

        public AbstractTool CreateTool() => new RectangleTool(application);

        public void Initialize(Painter.Painter application) => this.application = application;
    }
}
