﻿using Painter.Graphics;
using Painter.PluginHandlers;
using Painter.PluginHosts;
using Painter.Plugins;
using Painter.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Painter
{
    public partial class Painter : Form
    {
        private const String DefaultPluginSubDirectory = "plugins";
        private PluginLoader pluginLoader;
        public Canvas Canvas { get; private set; }
        public Toolbar Toolbar { get; private set; }
        public ShapeCollection ShapeCollection { get; private set; } = new ShapeCollection();
        public SerializerDeserializerRegistry SerializerDeserializerRegistry { get; private set; } = new SerializerDeserializerRegistry();
        public ShapeTypesRegistry ShapeTypesRegistry { get; private set; } = new ShapeTypesRegistry();


        public Painter()
        {
            InitializeComponent();
            InitializeSubComponents();
            SetupHandlers();
            InitializePluginLoader();
        }

        private void SetupHandlers()
        {
            ShapeCollection.CollectionChanged += (sender, e) =>
            {
                Canvas.DrawingLayer.Clear();
                Canvas.Refresh();
            };
            Canvas.DrawingLayer.RedrawEvent += (sender, e) =>
            {
                e.Graphics.Clear(System.Drawing.Color.White);
                ShapeCollection.Draw(e.Graphics);
            };
            Toolbar.ChangeToolEvent += (sender, e) => Canvas.UILayer.Clear();

            SerializerDeserializerRegistry.Add(new JsonSerializerDeserializer(ShapeTypesRegistry));
        }

        private void panelToolbar_Paint(object sender, PaintEventArgs e)
        {

        }

        private void InitializeSubComponents()
        {
            Toolbar = new Toolbar();
            Canvas = new Canvas();

            Canvas.Dock = DockStyle.Fill;
            Toolbar.Dock = DockStyle.Fill;

            panelToolbar.Controls.Add(Toolbar);
            drawingPanel.Controls.Add(Canvas);
        }

        private void InitializePluginLoader()
        {
            pluginLoader = new PluginLoader();
            pluginLoader.Directories.Add(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), DefaultPluginSubDirectory));
            pluginLoader.PluginHandlers.Add(typeof(IToolPlugin), new ToolPluginHost(this));
            pluginLoader.PluginHandlers.Add(typeof(IShapePlugin), new ShapePluginHost(ShapeTypesRegistry));
            pluginLoader.LoadAllPlugins();
        }

        private void menuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var saver = new Saver(this);
            saver.Save();
        }

        private void loadToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var loader = new Loader(this);
            loader.Load();
        }
    }
}
