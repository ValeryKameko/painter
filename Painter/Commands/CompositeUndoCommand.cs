﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Painter.Commands
{
    public class CompositeUndoCommand : List<IUndoCommand>, IUndoCommand
    {
        public void Execute()
        {
            ForEach(command => command.Execute());
        }

        public void Undo()
        {
            foreach (var command in this.AsEnumerable().Reverse())
                command.Undo();
        }
    }
}
