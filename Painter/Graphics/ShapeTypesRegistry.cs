﻿using Painter.Graphics;
using Painter.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Painter
{
    public class ShapeTypesRegistry
    {
        public List<Type> ShapeTypes { get; private set; }

        public ShapeTypesRegistry()
        {
            ShapeTypes = new List<Type>();
        }

        public void AddShapeType(Type type)
        {
            if (typeof(AbstractShape).IsAssignableFrom(type))
                ShapeTypes.Add(type);
        }
    }
}
