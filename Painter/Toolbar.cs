﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Painter.Graphics;
using Painter.Plugins;

namespace Painter
{
    public class Toolbar : FlowLayoutPanel
    {
        private List<ToolButton> buttons;

        public AbstractTool CurrentTool { get; private set; }

        public event EventHandler ChangeToolEvent;

        public Toolbar()
        {
            buttons = new List<ToolButton>();
            this.Resize += (sender, e) => Self_OnResize((Toolbar) sender, e);
        }

        public void AddTool(AbstractTool tool)
        {
            var button = new ToolButton(tool);
            buttons.Add(button);
            Controls.Add(button);
            tool.EnableEvent += (sender, e) => HandleDrawerSelect(tool);
            button.Width = this.Width;
        }

        private void HandleDrawerSelect(AbstractTool tool)
        {
            if (tool == CurrentTool)
                return;
            CurrentTool?.Disable();
            CurrentTool = tool;
            ChangeToolEvent?.Invoke(this, EventArgs.Empty);
        }

        public void Self_OnResize(Toolbar sender, EventArgs e)
        {
            buttons.ForEach(button => button.Width = this.Width);
            Refresh();
        }
    }
}
