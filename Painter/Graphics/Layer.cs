﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Painter
{
    public class Layer
    {
        private Bitmap image;
        public float Width { get; private set; }
        public float Height { get; private set; }

        public bool IsCleared { get; private set; }

        public event EventHandler<RedrawEventArgs> RedrawEvent;
        public event EventHandler RedrawedEvent;
        public event EventHandler<ResizeEventArgs> ResizeEvent;

        public Layer(float width, float height)
        {
            Resize(width, height);
        }

        public void Resize(float width, float height)
        {
            Width = width;
            Height = height;
            image?.Dispose();

            image = new Bitmap((int)width, (int)height);

            var args = new ResizeEventArgs();
            args.Height = Height;
            args.Width = Width;

            IsCleared = true;
            ResizeEvent?.Invoke(this, args);
        }

        public void Redraw()
        {
            using (var graphics = System.Drawing.Graphics.FromImage(image)) {
                var args = new RedrawEventArgs();
                args.Graphics = graphics;
                RedrawEvent?.Invoke(this, args);
            }
            IsCleared = false;
            RedrawedEvent?.Invoke(this, EventArgs.Empty);
        }

        public void Draw(System.Drawing.Graphics graphics)
        {
            if (image != null)
                graphics.DrawImage(image, 0, 0);
        }

        public void Clear()
        {
            IsCleared = true;
        }
    }

    public class RedrawEventArgs : EventArgs
    {
        public System.Drawing.Graphics Graphics { get; set; }
    }

    public class ResizeEventArgs : EventArgs
    {
        public float Width { get; set; }
        public float Height { get; set; }
    }
}
