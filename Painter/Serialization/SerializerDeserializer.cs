﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Painter
{
    public abstract class SerializerDeserializer
    {
        public abstract void Serialize(ShapeCollection shapeCollection, Stream stream);
        public abstract ShapeCollection Deserialize(Stream stream);

        public abstract String Warning { get; }
        public abstract bool WasWarning { get; }


        public abstract String Error { get; }
        public abstract bool WasError { get; }

        public abstract String Name { get; }
        public abstract String Desctiption { get; }
        public abstract IEnumerable<String> Extensions { get; }
    }
}
