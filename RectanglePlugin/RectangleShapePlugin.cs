﻿using Painter.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RectanglePlugin
{
    public class RectangleShapePlugin : IShapePlugin
    {
        public Type ShapeType => typeof(Rectangle);
    }
}
