﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Painter.Plugins
{
    public interface IToolPlugin : IPlugin
    {
        void Initialize(Painter application);
        AbstractTool CreateTool();
    }
}
