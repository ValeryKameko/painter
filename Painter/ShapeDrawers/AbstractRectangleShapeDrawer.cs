﻿using Painter.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Painter.ShapeDrawers
{
    public abstract class AbstractRectangleShapeDrawer : AbstractShapeDrawer
    {
        private Point firstPoint;
        private Point secondPoint;

        private bool isDrawingComplete;

        public override event EventHandler DrawingCompleteEvent;

        public override bool IsDrawingComplete => isDrawingComplete;

        public override AbstractShape PartialShape {
            get
            {
                if (firstPoint == null || secondPoint == null)
                    return null;
                Point leftTop = CalculateLeftTop();
                Point rightBottom = CalculateRightBottom();
                return CreatePartialShape(leftTop, rightBottom);
            }
        }

        protected abstract AbstractShape CreatePartialShape(Point leftTop, Point rightBottom);

        public override AbstractShape CompleteShape
        {
            get
            {
                if (firstPoint == null || secondPoint == null)
                    return null;
                Point leftTop = CalculateLeftTop();
                Point rightBottom = CalculateRightBottom();
                return CreateCompleteShape(leftTop, rightBottom);
            }
        }

        protected abstract AbstractShape CreateCompleteShape(Point leftTop, Point rightBottom);

        public AbstractRectangleShapeDrawer()
        {
            isDrawingComplete = false;
        }

        public override void Down(float x, float y)
        {
            firstPoint = new Point(x, y);
            secondPoint = new Point(x, y);
        }

        public override void Up(float x, float y)
        {
            if (firstPoint == null || secondPoint == null)
                return;
            Move(x, y);
            isDrawingComplete = true;
            DrawingCompleteEvent?.Invoke(this, EventArgs.Empty);
        }

        public override void Move(float x, float y)
        {
            secondPoint = new Point(x, y);
        }

        public override void Flush()
        {
            firstPoint = null;
            secondPoint = null;
            isDrawingComplete = false;
        }

        private Point CalculateLeftTop()
        {
            float left = Math.Min(firstPoint.X, secondPoint.X);
            float top = Math.Min(firstPoint.Y, secondPoint.Y);
            return new Point(left, top);
        }

        private Point CalculateRightBottom()
        {
            float right = Math.Max(firstPoint.X, secondPoint.X);
            float bottom = Math.Max(firstPoint.Y, secondPoint.Y);
            return new Point(right, bottom);
        }
    }
}
