﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Painter.Serialization
{
    public class Saver
    {
        private Painter application;

        public Saver(Painter application)
        {
            this.application = application;
        }

        public void Save()
        {
            var dialog = new SaveFileDialog();
            dialog.Filter = CreateFileFilter();
            dialog.InitialDirectory = Application.StartupPath;
            var result = dialog.ShowDialog();
            if (result != DialogResult.OK)
                return;
            try
            {
                using (var stream = new FileStream(dialog.FileName, FileMode.Create))
                {
                    var serializer = application.SerializerDeserializerRegistry.ElementAt(dialog.FilterIndex - 1);
                    serializer.Serialize(application.ShapeCollection, stream);
                    if (serializer.WasError)
                    {
                        MessageBox.Show(FormatSaveError(serializer.Error), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        if (serializer.WasWarning)
                            MessageBox.Show("Warning during save loading: " + serializer.Warning, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch (SecurityException)
            {
                MessageBox.Show(FormatSaveError("Not enough permissions"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show(FormatSaveError("Unauthorized access"), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private String FormatSaveError(String error)
        {
            return "Error while saving: " + error;
        }

        private String CreateFileFilter()
        {
            var builder = new StringBuilder();
            foreach (var entry in application.SerializerDeserializerRegistry)
            {
                builder.Append(entry.Desctiption);
                builder.Append("|");
                builder.Append(String.Join(";", entry.Extensions.Select(extension => $"*.{extension}")));
            }
            return builder.ToString();
        }
    }
}
