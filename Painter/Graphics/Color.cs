﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Painter.Graphics
{
    [DataContract]
    public class Color
    {
        [DataMember(Order = 0)]
        public float R { get; set; }
        [DataMember(Order = 1)]
        public float G { get; set; }
        [DataMember(Order = 2)]
        public float B { get; set; }
        [DataMember(Order = 3)]
        public float A { get; set; }

        public Color(float r, float g, float b, float a)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }

        public Color(float r, float g, float b)
            : this(r, g, b, 1.0f)
        {
        }

        public Color()
            : this(0.0f, 0.0f, 0.0f, 1.0f)
        {

        }

        public static Color FromDrawingColor(System.Drawing.Color color)
        {
            return new Color(color.R / 255f, color.G / 255f, color.B / 255f, color.A / 255f);
        }

        public System.Drawing.Color ToDrawingColor()
        {
            return System.Drawing.Color.FromArgb(
                (int)Math.Round(A * 255),
                (int)Math.Round(R * 255),
                (int)Math.Round(G * 255),
                (int)Math.Round(B * 255));
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            foreach (var value in new List<float>{ R, G, B, A })
            {
                if (value < 0 || value > 1)
                    throw new SerializationException("Incorrect color data");
            }
        }
    }
}
