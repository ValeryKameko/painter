﻿using Painter.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Painter.PluginHandlers
{
    public class ToolPluginHost : IPluginHost
    {
        private Painter application;

        public ToolPluginHost(Painter application)
        {
            this.application = application;
        }

        public void AcceptPlugin(Type pluginType)
        {
            if (!typeof(IToolPlugin).IsAssignableFrom(pluginType))
                return;
            var constructor = pluginType.GetConstructor(Type.EmptyTypes);
            try
            {
                var obj = constructor?.Invoke(null);
                var plugin = obj as IToolPlugin;
                plugin.Initialize(application);
                var tool = plugin.CreateTool();
                application.Toolbar.AddTool(tool);
            }
            catch (Exception e)
            {
                MessageBox.Show(
                    $"Error occured while loading tool plugin {pluginType.Name} from {pluginType.Assembly.FullName}", 
                    "Error", 
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Error);
            }
        }
    }
}
