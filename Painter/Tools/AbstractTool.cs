﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Painter
{
    public abstract class AbstractTool
    {
        public void Enable()
        {
            if (Enabled)
                return;
            Enabled = true;
            OnEnable();
            EnableEvent?.Invoke(this, EventArgs.Empty);
        }

        public abstract void OnEnable();

        public void Disable()
        {
            if (!Enabled)
                return;
            DisableEvent?.Invoke(this, EventArgs.Empty);
            OnDisable();
            Enabled = false;
        }

        public abstract void OnDisable();

        public abstract String Name { get; }

        public event EventHandler EnableEvent;
        public event EventHandler DisableEvent;
        public bool Enabled { get; private set; }
    }
}
