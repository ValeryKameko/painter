﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Painter.Commands
{
    public class CommandHistory
    {
        private Stack<IUndoCommand> beforeStack;
        private Stack<IUndoCommand> afterStack;

        public CommandHistory()
        {
            beforeStack = new Stack<IUndoCommand>();
            afterStack = new Stack<IUndoCommand>();
        }

        public void Undo()
        {
            var command = beforeStack.Pop();
            if (command != null)
            {
                command.Undo();
                afterStack.Push(command);
            }
        }

        public void Redo()
        {
            var command = afterStack.Pop();
            if (command != null)
            {
                command.Execute();
                beforeStack.Push(command);
            }
        }

        public void ExecuteCommand(IUndoCommand command)
        {
            command.Execute();
            beforeStack.Push(command);
            afterStack.Clear();
        }
    }
}
