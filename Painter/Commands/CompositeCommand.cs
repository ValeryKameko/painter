﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Painter.Commands
{
    public class CompositeCommand : List<ICommand>, ICommand
    {
        public void Execute() => ForEach(command => command.Execute());
    }
}
