﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Runtime.Serialization;

namespace Painter.Graphics
{
    [DataContract]
    public abstract class AbstractShape : ICloneable
    {
        [DataMember]
        public Pen Pen { get; set; }

        public abstract object Clone();

        public abstract Point Origin { get; set; }
        public abstract BoundingBox BoundingBox { get; }

        public abstract void Draw(System.Drawing.Graphics graphics);
    }
}
