﻿using System;
using Painter;
using System.Windows.Forms;
using Painter.Graphics;
using System.Collections.ObjectModel;
using System.Linq;
using System.Collections.Generic;

namespace MovePlugin
{
    public class MoveTool : AbstractTool
    {
        private enum State
        {
            Initial,
            Selecting,
            Selected,
            Moving
        }

        private State state;
        private Canvas Canvas;

        private ShapeCollection shapeCollection;
        private List<Tuple<AbstractShape, Point>> movingObjects;

        private Point selectionStartPoint;
        private Point selectionEndPoint;

        private Point movingStartPoint;
        private Point movingEndPoint;

        public override string Name => "Move";

        public MoveTool(Painter.Painter application)
        {
            Canvas = application.Canvas;
            shapeCollection = application.ShapeCollection;
            state = State.Initial;
        }

        public override void OnDisable()
        {
            Canvas.MouseMove -= Canvas_MouseMove;
            Canvas.MouseDown -= Canvas_MouseDown;
            Canvas.MouseUp -= Canvas_MouseUp;
            Canvas.UILayer.RedrawEvent -= UILayer_RedrawEvent;
            if (state == State.Moving && movingObjects != null)
            {
                foreach (var shape in movingObjects)
                    shapeCollection.Add(shape.Item1);
            }
            UpdateUI();
            movingObjects = null;
            Console.WriteLine("Move plugin disabled");
        }

        public override void OnEnable()
        {
            Canvas.MouseMove += Canvas_MouseMove;
            Canvas.MouseDown += Canvas_MouseDown;
            Canvas.MouseUp += Canvas_MouseUp;
            Canvas.UILayer.RedrawEvent += UILayer_RedrawEvent;
            UpdateUI();
            movingObjects = null;
            state = State.Initial;
            Console.WriteLine("Move plugin enabled");
        }

        private void DrawUIRectangle(System.Drawing.Graphics graphics, Point from, Point to)
        {
            var left = Math.Min(from.X, to.X);
            var right = Math.Max(from.X, to.X);
            var top = Math.Min(from.Y, to.Y);
            var bottom = Math.Max(from.Y, to.Y);

            var leftTop = new System.Drawing.PointF(left, top);
            var rightTop = new System.Drawing.PointF(right, top);
            var leftBottom = new System.Drawing.PointF(left, bottom);
            var rightBottom = new System.Drawing.PointF(right, bottom);

            var uiPen = new System.Drawing.Pen(System.Drawing.Color.Gray)
            {
                DashStyle = System.Drawing.Drawing2D.DashStyle.Dash,
                Width = 2
            };
            graphics.DrawLine(uiPen, leftTop, rightTop);
            graphics.DrawLine(uiPen, rightTop, rightBottom);
            graphics.DrawLine(uiPen, rightBottom, leftBottom);
            graphics.DrawLine(uiPen, leftBottom, leftTop);
        }

        private void UILayer_RedrawEvent(object sender, RedrawEventArgs e)
        {
            e.Graphics.Clear(System.Drawing.Color.Transparent);
            if (state == State.Moving || state == State.Selected)
            {
                movingObjects.ForEach(entry => entry.Item1.Draw(e.Graphics));
            }
            if (state == State.Selecting || state == State.Selected)
                DrawUIRectangle(e.Graphics, selectionStartPoint, selectionEndPoint);
        }

        private void Canvas_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (state == State.Moving)
                {
                    movingEndPoint = new Point(e.X, e.Y);
                    foreach (var shape in movingObjects)
                        shapeCollection.Add(shape.Item1);
                    movingObjects = null;
                    state = State.Initial;
                }
                else if (state == State.Selecting)
                {
                    selectionEndPoint = new Point(e.X, e.Y);
                    movingObjects = shapeCollection.Where(shape =>
                    {
                        var selectableObject = shape as ISelectable;
                        if (selectableObject == null)
                            return false;
                        return selectableObject.IsSelected(
                            Math.Min(selectionStartPoint.X, selectionEndPoint.X),
                            Math.Min(selectionStartPoint.Y, selectionEndPoint.Y),
                            Math.Max(selectionStartPoint.X, selectionEndPoint.X),
                            Math.Max(selectionStartPoint.Y, selectionEndPoint.Y));
                    }).Select(shape => new Tuple<AbstractShape, Point>(shape, shape.Origin)).ToList();

                    if (movingObjects.Count > 0)
                    {
                        state = State.Selected;
                    }
                    else
                        state = State.Initial;
                }
                UpdateUI();
                UpdateDrawing();
            }
        }

        private void Canvas_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (state == State.Initial)
                {
                    var selectedShape = shapeCollection.Reverse().FirstOrDefault(shape =>
                    {
                        var selectableObject = shape as ISelectable;
                        if (selectableObject == null)
                            return false;
                        return selectableObject.IsSelected(e.X, e.Y);
                    });
                    if (selectedShape != null)
                    {
                        movingObjects = new List<Tuple<AbstractShape, Point>>() { new Tuple<AbstractShape, Point>(selectedShape, selectedShape.Origin) };
                        shapeCollection.Remove(selectedShape);
                        movingStartPoint = new Point(e.X, e.Y);
                        movingEndPoint = movingStartPoint;
                        state = State.Moving;
                        Console.WriteLine("[Move plugin] hit");
                    }
                    else
                    {
                        selectionStartPoint = new Point(e.X, e.Y);
                        selectionEndPoint = selectionStartPoint;
                        state = State.Selecting;
                        Console.WriteLine("[Move plugin] start selecting");
                    }
                }
                else if (state == State.Selected)
                {
                    bool hit = movingObjects.Count(shape =>
                    {
                        return (shape.Item1 as ISelectable).IsSelected(e.Location.X, e.Location.Y);
                    }) > 0;
                    if (!hit)
                    {
                        movingObjects = null;
                        state = State.Initial;
                        Console.WriteLine("[Move plugin] moving canceled");
                    }
                    else
                    {
                        movingObjects.ForEach(entry => shapeCollection.Remove(entry.Item1));
                        movingStartPoint = new Point(e.X, e.Y);
                        movingEndPoint = new Point(e.X, e.Y);
                        state = State.Moving;
                        Console.WriteLine("[Move plugin] start moving");
                    }
                }
                UpdateUI();
                UpdateDrawing();
            }
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (state == State.Moving)
                {
                    movingEndPoint = new Point(e.X, e.Y);
                    movingObjects.ForEach(entry =>
                    {
                        entry.Item1.Origin = new Point(
                            entry.Item2.X - movingStartPoint.X + movingEndPoint.X,
                            entry.Item2.Y - movingStartPoint.Y + movingEndPoint.Y);
                    });
                    Console.WriteLine($"[Move plugin] Move from {movingStartPoint.X} {movingStartPoint.Y} to {movingEndPoint.X} {movingEndPoint.Y}");
                    UpdateUI();
                }
                else if (state == State.Selecting)
                {
                    selectionEndPoint = new Point(e.X, e.Y);
                    Console.WriteLine($"[Move plugin] Selecting from {movingStartPoint} to {movingEndPoint}");
                    UpdateUI();
                }
            }
        }

        private void UpdateUI()
        {
            Canvas.UILayer.Clear();
            Canvas.Refresh();
        }

        private void UpdateDrawing()
        {
            Canvas.DrawingLayer.Clear();
            Canvas.Refresh();
        }
    }
}