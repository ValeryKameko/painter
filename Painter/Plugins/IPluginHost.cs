﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Painter.Plugins
{
    public interface IPluginHost
    {
        void AcceptPlugin(Type pluginType);
    }
}
