﻿namespace Painter.Graphics
{
    public class BoundingBox
    {
        public float Left { get; private set; }
        public float Right { get; private set; }
        public float Top { get; private set; }
        public float Bottom { get; private set; }

        public BoundingBox(float left, float bottom, float right, float top)
        {
            Left = left;
            Right = right;
            Top = top;
            Bottom = left;
        }
    }
}