﻿using Painter.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Painter.Tools
{
    public abstract class AbstractShapeDrawerTool : AbstractTool
    {
        protected Canvas Canvas { get; private set; }
        protected AbstractShapeDrawer Drawer { get; private set; }
        protected ShapeCollection ShapeCollection { get; private set; }

        public AbstractShapeDrawerTool(AbstractShapeDrawer drawer, Painter application)
        {
            Canvas = application.Canvas;
            ShapeCollection = application.ShapeCollection;
            Drawer = drawer;
            drawer.Pen = new Pen(new Color(0.5f, 0.5f, 0.5f), 4);
        }

        public override void OnDisable()
        {
            Canvas.MouseMove -= Canvas_MouseMove;
            Canvas.MouseDown -= Canvas_MouseDown;
            Canvas.MouseUp -= Canvas_MouseUp;
            Canvas.UILayer.RedrawEvent -= UILayer_RedrawEvent;
            Drawer.DrawingCompleteEvent -= Drawer_DrawingCompleteEvent;
            Canvas.UILayer.Clear();
        }

        private void UILayer_RedrawEvent(object sender, RedrawEventArgs e)
        {
            e.Graphics.Clear(System.Drawing.Color.Transparent);
            var shape = Drawer.PartialShape;
            shape?.Draw(e.Graphics);
        }

        public override void OnEnable()
        {
            Drawer.Flush();
            Canvas.MouseMove += Canvas_MouseMove;
            Canvas.MouseDown += Canvas_MouseDown;
            Canvas.MouseUp += Canvas_MouseUp;
            Canvas.UILayer.RedrawEvent += UILayer_RedrawEvent;
            Drawer.DrawingCompleteEvent += Drawer_DrawingCompleteEvent;
            Canvas.UILayer.Clear();
        }

        private void Drawer_DrawingCompleteEvent(object sender, EventArgs e)
        {
            ShapeCollection.Add(Drawer.CompleteShape);
            Canvas.UILayer.Clear();
        }

        private void Canvas_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Drawer.Up(e.X, e.Y);
                UpdateUI();
            }
        }

        private void Canvas_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Drawer.Down(e.X, e.Y);
                UpdateUI();
            }
        }

        private void Canvas_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Drawer.Move(e.X, e.Y);
                UpdateUI();
            }
        }

        private void UpdateUI()
        {
            Canvas.UILayer.Clear();
            Canvas.Refresh();
        }
    }
}
