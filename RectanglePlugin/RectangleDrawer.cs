﻿using Painter.ShapeDrawers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Painter.Graphics;

namespace RectanglePlugin
{
    public class RectangleDrawer : AbstractRectangleShapeDrawer
    {
        protected override AbstractShape CreateCompleteShape(Point leftTop, Point rightBottom)
        {
            var shape = new Rectangle(leftTop, rightBottom);
            shape.Pen = Pen;
            return shape;
        }

        protected override AbstractShape CreatePartialShape(Point leftTop, Point rightBottom)
        {
            var shape = new Rectangle(leftTop, rightBottom);
            shape.Pen = Pen;
            return shape;
        }
    }
}
