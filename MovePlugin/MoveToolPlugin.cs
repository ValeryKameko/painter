﻿using Painter.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Painter;

namespace MovePlugin
{
    public class MoveToolPlugin : IToolPlugin
    {
        private Painter.Painter application;

        public AbstractTool CreateTool() => new MoveTool(application);

        public void Initialize(Painter.Painter application) => this.application = application;
    }
}
