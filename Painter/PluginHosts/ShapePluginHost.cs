﻿using Painter.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Painter.PluginHosts
{
    public class ShapePluginHost : IPluginHost
    {
        private ShapeTypesRegistry registry;

        public ShapePluginHost(ShapeTypesRegistry registry)
        {
            this.registry = registry;
        }

        public void AcceptPlugin(Type pluginType)
        {
            if (!typeof(IShapePlugin).IsAssignableFrom(pluginType))
                return;
            var constructor = pluginType.GetConstructor(Type.EmptyTypes);
            try
            {
                var obj = constructor?.Invoke(null);
                var plugin = obj as IShapePlugin;
                registry.AddShapeType(plugin.ShapeType);
            }
            catch (Exception e)
            {
                MessageBox.Show(
                    $"Error occured while loading shape plugin {pluginType.Name} from {pluginType.Assembly.FullName}",
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

    }
}
