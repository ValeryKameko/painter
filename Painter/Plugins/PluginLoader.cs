﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using Painter.Plugins;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Painter
{
    public class PluginLoader
    {
        private const String PLUGIN_EXTENSION = "dll";
        private const String PLUGIN_PATTERN = "*." + PLUGIN_EXTENSION;

        public List<String> Directories { get; private set; }
        public Dictionary<Type, IPluginHost> PluginHandlers { get; private set; }
        public PluginLoader()
        {
            Directories = new List<String>();
            PluginHandlers = new Dictionary<Type, IPluginHost>();
        }

        public void LoadAllPlugins()
        {
            foreach (var pluginDirectory in Directories)
            {
                foreach (var pluginPath in Directory.EnumerateFiles(pluginDirectory, PLUGIN_PATTERN, SearchOption.AllDirectories))
                {
                    try
                    {
                        LoadPlugin(pluginPath);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(
                            String.Format("Plugin {0} has not been loaded because of exception \"{1}\"", Regex.Match(pluginPath, @"^(.*)[/\\]([^\\/]+)\.dll$").Groups[2], e.Message),
                            "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
        }

        public void LoadPlugin(String path)
        {
            var pluginAssembly = Assembly.LoadFile(path);
            var pluginTypes = from type in pluginAssembly.ExportedTypes where type.GetInterfaces().Contains(typeof(IPlugin)) select type;

            foreach (var pluginType in pluginTypes)
            {
                foreach (var entry in PluginHandlers)
                {
                    if (entry.Key.IsAssignableFrom(pluginType))
                        entry.Value.AcceptPlugin(pluginType);
                }
            }
        }
    }
}
