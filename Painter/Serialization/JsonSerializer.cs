﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using Painter.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Painter.Serialization
{
    public class JsonSerializerDeserializer : SerializerDeserializer
    {
        private class KnownTypesBinder : ISerializationBinder
        {
            public IList<Type> KnownTypes { get; set; }

            public KnownTypesBinder(IList<Type> knownTypes)
            {
                this.KnownTypes = knownTypes;
            }

            public void BindToName(Type serializedType, out string assemblyName, out string typeName)
            {
                assemblyName = null;
                typeName = serializedType.Name;
            }

            public Type BindToType(string assemblyName, string typeName)
            {
                return KnownTypes.SingleOrDefault(type => type.Name == typeName);
            }
        }

        private ShapeTypesRegistry registry;

        public JsonSerializerDeserializer(ShapeTypesRegistry registry)
        {
            this.registry = registry;
        }

        public override String Name => "JSON";

        private IEnumerable<String> extensions = new List<String> { "json" };
        public override IEnumerable<String> Extensions => extensions;

        public override String Desctiption => "JSON format";

        private String warning;
        public override string Warning => warning;

        private bool wasWarning;
        public override bool WasWarning => wasWarning;

        private String error;
        public override string Error => error;

        private bool wasError;
        public override bool WasError => wasError;

        public override ShapeCollection Deserialize(Stream stream)
        {
            wasError = false;
            wasWarning = false;
            var serializer = new JsonSerializer()
            {
                SerializationBinder = new KnownTypesBinder(registry.ShapeTypes),
                TypeNameHandling = TypeNameHandling.Auto
            };
            serializer.Error += (sender, e) =>
            {
                if (new Regex(@"^\[\d+\]\.\$type$").IsMatch(e.ErrorContext.Path))
                {
                    wasWarning = true;
                    warning = "Unknown shape found";
                    e.ErrorContext.Handled = true;
                }
            };
            using (var streamReader = new StreamReader(stream))
            using (var jsonTextReader = new JsonTextReader(streamReader))
            {
                try
                {
                    var shapeCollection = (ShapeCollection) serializer.Deserialize(jsonTextReader, typeof(ShapeCollection));
                    return shapeCollection;
                }
                catch (SerializationException e)
                {
                    wasError = true;
                    error = "Wrong saving format";
                    return null;
                }
            }
        }

        public override void Serialize(ShapeCollection shapeCollection, Stream stream)
        {
            var serializer = new JsonSerializer()
            {
                SerializationBinder = new KnownTypesBinder(registry.ShapeTypes),
                TypeNameHandling = TypeNameHandling.Auto
            };
            using (var streamWriter = new StreamWriter(stream))
            using (var jsonTextWriter = new JsonTextWriter(streamWriter))
            {
                serializer.Serialize(jsonTextWriter, shapeCollection, typeof(ShapeCollection));
            }
        }
    }

    public class ErrorEventArgs : EventArgs
    {
        public Exception Error { get; set; }
    }
}
